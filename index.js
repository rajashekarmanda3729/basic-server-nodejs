const http = require('http')
const fs = require('fs')
const path = require('path')

// 1> Hello world server 

// http.createServer((req, res) => {
//     console.log(req)
//     res.write('Hello Backend World !')
//     res.end()
// }).listen(5000, () => {
//     console.log('server running')
// })

// 2> creating a HTML-contente server

// const server = http.createServer((req, res) => {
//     console.log(req)
//     if (req.url == '/') {
//         res.writeHead(200, { 'Content-Type': 'text/html' })
//         res.write('Hello Backend World !')
//         res.end('<h1>Home</h1>')
//     }
//     if (req.url == '/about') {
//         res.writeHead(200, { 'Content-Type': 'text/html' })
//         res.end('<h1>About</h1>')
//     }
// })

// const PORT = process.env.PORT | 5000

// server.listen(PORT, () => {
//     console.log('server running')
// })


// 3> Creating a server html pages show 

// const server = http.createServer((req, res) => {
//     if (req.url == '/') {
//         fs.readFile('public/index.html', (err, data) => {
//             if (err) throw err
//             res.writeHead(200, { 'Content-Type': 'text/html' })
//             res.end(data)
//         })
//     }
//     if (req.url == '/about') {
//         fs.readFile('public/about.html', (err, data) => {
//             if (err) throw err
//             res.writeHead(200, { 'Content-Type': 'text/html' })
//             res.end(data)
//         })
//     }
// })
// const PORT = process.env.PORT | 5000
// server.listen(PORT, () => {
//     console.log('server running')
// })

// 4> Creating a REST-api server containing data

// const users = [
//     {
//         id: 1,
//         name: 'Rajashekar',
//         age: 25
//     },
//     {
//         id: 2,
//         name: 'John Dow',
//         age: 23
//     },
//     {
//         id: 3,
//         name: 'Wrestle',
//         age: 33
//     },

// ]

// const server = http.createServer((req, res) => {
//     if (req.url == '/') {
//         res.writeHead(200, { 'Content-Type': 'text/html' })
//         res.end('<h1>Home</h1>')
//     }
//     if (req.url == '/api/users') {
//         res.writeHead(200, { 'Content-Type': 'application/json' })
//         res.end(JSON.stringify(users))
//     }
//     if (req.url == '/api/users/:id') {
//         console.log(req)
//         const urlArr = req.url.split('/')
//         const userId = urlArr[urlArr.length - 1]
//         const user = users.find(eachUser => eachUser.id == userId)
//         res.writeHead(200, { 'Content-Type': 'application/json' })
//         res.end(JSON.stringify(user))
//     }
// })
// const PORT = process.env.PORT | 5000
// server.listen(PORT, () => {
//     console.log('server running')
// })


// 5> Server all-types of files & error's

const server = http.createServer((req, res) => {
    let filePath = path.join(
        __dirname,
        "public",
        req.url === "/" ? "index.html" : req.url
    );

    // Extension of file
    let extname = path.extname(filePath);

    // Initial content type
    let contentType = "text/html";

    // Check ext and set content type
    switch (extname) {
        case ".js":
            contentType = "text/javascript";
            break;
        case ".css":
            contentType = "text/css";
            break;
        case ".json":
            contentType = "application/json";
            break;
        case ".png":
            contentType = "image/png";
            break;
        case ".jpg":
            contentType = "image/jpg";
            break;
    }

    // Check if contentType is text/html but no .html file extension

    if (contentType == "text/html" && extname == "") filePath += ".html";

    // log the filePath

    console.log(filePath);

    // Read File

    fs.readFile(filePath, (err, content) => {
        if (err) {
            if (err.code == "ENOENT") {
                // Page not found
                fs.readFile(
                    path.join(__dirname, "public", "404.html"),
                    (err, content) => {
                        res.writeHead(404, { "Content-Type": "text/html" });
                        res.end(content, "utf8");
                    }
                );
            } else {
                //  Some server error
                res.writeHead(500);
                res.end(`Server Error: ${err.code}`);
            }
        } else {
            // Success
            res.writeHead(200, { "Content-Type": contentType });
            res.end(content, "utf8");
        }
    });
});

const PORT = 5000;

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));